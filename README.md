# Develop KSQL UDF

## Download Confluent Platform Quick Start (Local)

Download from https://docs.confluent.io/current/quickstart/ce-quickstart.html#ce-quickstart and follow the guide.

## Install Development Dependencies

Install OpenJDK.
```bash
sudo apt install openjdk-8-jdk-headless
```

Install SDKMan
```bash
curl -s "https://get.sdkman.io" | bash
source "/home/tibrahim/.sdkman/bin/sdkman-init.sh"
```

Install Maven.
```bash
sdk install maven
```

Check Maven version.
```bash
mvn -version
```

Next, add the Maven repositories from Confluent to your `~/.m2/settings.xml` file:

Create file if not exists:
```bash
touch ~/.m2/settings.xml
```

Fill with this:
```xml
<settings>
   <profiles>
      <profile>
         <id>myprofile</id>
         <repositories>
            <!-- Confluent releases -->  
            <repository>
               <id>confluent</id>
               <url>https://packages.confluent.io/maven/</url>
            </repository>

            <!-- further repository entries here -->
         </repositories>   
      </profile>
    </profiles> 

   <activeProfiles>
      <activeProfile>myprofile</activeProfile>
   </activeProfiles>
</settings>
```

## Create the Source Code Directory

Package name is `com.bizzycoid.ksql.udf`.

Run this to bootstrap project directory:
```bash
mvn archetype:generate -X \
    -DarchetypeGroupId=io.confluent.ksql \
    -DarchetypeArtifactId=ksql-udf-quickstart \
    -DarchetypeVersion=5.4.0
```
There will be lots off automated command line process runnings. Grab a coffee, this would take some times.

You will be asked to provide some information about your project.

```bash
Define value for property 'groupId':  com.bizzy.ksql.udf
Define value for property 'artifactId':  bizzy-ksql-udf
Define value for property 'version':  0.1.0
Define value for property 'package':  com.bizzy.ksql.udf
Define value for property 'author':  Taufiq Ibrahim
```

```bash
mkdir -p src/main/java/com/bizzy/ksql/udf
```

Building the package using:
```bash
mvn clean package
```

## Example: Remove Leading Zeros

Here, we will guide you on how to implement RemoveLeadingZeros function. This function is already shipped in this repository and this example is how we made it.

First, we create a file called `RemoveLeadingZeros.java` in `src/main/java/com/bizzy/ksql/udf` directory.

This is the code:
```java
package com.bizzy.ksql.udf;

import io.confluent.ksql.function.udf.Udf;
import io.confluent.ksql.function.udf.UdfDescription;

@UdfDescription(
    name = "removeleadingzeros",
    description = "remove leading zeros from string",
    version = "0.1.0",
    author = "Taufiq Ibrahim"
)
public class RemoveLeadingZerosUdf {
    @Udf(description = "remove leading zeros from string")
    public String removeLeadingZeros(String s) {

        if (s == null) {
            return null;
        } else {
            StringBuilder sb = new StringBuilder(s);

            while (sb.length() > 1 && sb.charAt(0) == '0') {
                sb.deleteCharAt(0);
            }
    
            return sb.toString();
        }
    }
}
```

## Build the UDF Package
Use Maven to build the package and create a JAR. Copy the JAR to the ksql extensions directory.

In the root folder for your UDF, run Maven to build the package:

```bash
mvn clean package
```

## Deploy into KSQL Server

Upload the JARs into S3.
```bash
aws s3 cp target/bizzy-ksql-udf-0.1.0.jar s3://bizzy-group-data-engineer-public/ksql/udf/ --acl public-read
```

Create or re-create KSQL Server Docker container.
```bash
sudo docker-compose down && sudo docker-compose up -d
```
