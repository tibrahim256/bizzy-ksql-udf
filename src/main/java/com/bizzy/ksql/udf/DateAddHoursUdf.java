package com.bizzy.ksql.udf;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import io.confluent.ksql.function.udf.Udf;
import io.confluent.ksql.function.udf.UdfDescription;

@UdfDescription(
    name = "dateaddhours",
    description = "add hours to string formatted datetime",
    version = "0.1.0",
    author = "Taufiq Ibrahim"
)
public class DateAddHoursUdf {
    
    @Udf(description = "add hours to string formatted datetime")
    public String dateAddHours(final String ds, final String fmt, final Integer hours) {

        // Checking if input <null> then return <null>
        if (ds == null) {
            return null;
        } else {
            // Creating formatter
            DateTimeFormatter formatter = DateTimeFormat.forPattern(fmt);

            // Convert input string to Datetime usin Joda
            DateTime dt = DateTime.parse(ds, formatter);

            // Return added hours to Datetime and convert back to string
            return dt.plusHours(hours).toString(formatter);
        }

    }
}