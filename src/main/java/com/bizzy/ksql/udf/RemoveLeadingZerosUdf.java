package com.bizzy.ksql.udf;

import io.confluent.ksql.function.udf.Udf;
import io.confluent.ksql.function.udf.UdfDescription;

@UdfDescription(
    name = "removeleadingzeros",
    description = "remove leading zeros from string",
    version = "0.1.0",
    author = "Taufiq Ibrahim"
)
public class RemoveLeadingZerosUdf {
    @Udf(description = "remove leading zeros from string")
    public String removeLeadingZeros(String s) {

        if (s == null) {
            return null;
        } else {
            StringBuilder sb = new StringBuilder(s);

            while (sb.length() > 1 && sb.charAt(0) == '0') {
                sb.deleteCharAt(0);
            }
    
            return sb.toString();
        }
    }
}