package com.bizzy.ksql.udf;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/**
 * Example class that demonstrates how to unit test UDFs.
 */
public class DateAddHoursUdfTests {

  @ParameterizedTest(name = "dateAddHours({0}, {1}, {2})= {3}")
  @CsvSource(value = { "2012-01-01T08:31:00, yyyy-MM-dd'T'HH:mm:ss, 7, 2012-01-01T15:31:00",
      "2012-01-01T08:32:00.123, yyyy-MM-dd'T'HH:mm:ss.SSS, 7, 2012-01-01T15:32:00.123",
      "2012-01-01T08:00:00, yyyy-MM-dd'T'HH:mm:ss, 0, 2012-01-01T08:00:00",
      "2012-01-01T08:00:00, yyyy-MM-dd'T'HH:mm:ss, -1, 2012-01-01T07:00:00",
      "null, yyyy-MM-dd'T'HH:mm:ss, 7, null",
    }, nullValues = "null")
  void dateAddHours(final String ds, final String fmt, final Integer hours, final String expectedResult) {
    final DateAddHoursUdf dateAdd = new DateAddHoursUdf();
    final String actualResult = dateAdd.dateAddHours(ds, fmt, hours);
    assertEquals(expectedResult, actualResult,
        " dateAddHours (" + ds + "," + fmt + "," + hours + ") should equal " + expectedResult);
  }
}