package com.bizzy.ksql.udf;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/**
 * Example class that demonstrates how to unit test UDFs.
 */
public class RemoveLeadingZerosUdfTests {

  @ParameterizedTest(name = "removeLeadingZeros({0})= {1}")
  @CsvSource(value = {
      "0000000000000853212, 853212",
      "0853212, 853212",
      "world, world",
      "10001, 10001",
      "-10001, -10001",
      "-world, -world",
      "--world, --world",
      "null, null"
  }, nullValues = "null")
  void removeLeadingZeros(final String source, final String expectedResult) {
    final RemoveLeadingZerosUdf rem = new RemoveLeadingZerosUdf();
    final String actualResult = rem.removeLeadingZeros(source);
    assertEquals(expectedResult, actualResult, source + " removed leading zores should equal " + expectedResult);
  }
}